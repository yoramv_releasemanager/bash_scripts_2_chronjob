#!/bin/bash
# Author: yoram
# ------------------------------------------
function log()
 {
   echo $(date +%F-%H:%M:%S) $1 
   echo $(date +%F-%H:%M:%S) $1 >> $f_whitlabel_log 
 } 
prodrootpass="@B@cu5GW2017"
devrootpass="triltest"
workingpass=$prodrootpass
hostname=`hostname`
if [ $hostname = 'chefwork.pfslab.local' ];then
  workingpass=$devrootpass
fi
deploy_url="https://yoramv_releasemanager@bitbucket.org/yoramv_releasemanager/ansibe_scripts_2_chronjob.git"

git_url="https://software%40tradertools.com:proIT20!!@bitbucket.org/ivinitzer/yoram_testing.git"
ansible_root_folder="/tmp/yoram/testing/ansible_roles"
#ansible files location

f_inventory="${ansible_root_folder}/inventory"
f_main="${ansible_root_folder}/roles/lmpa_gw_conf_backup/tasks/main.yml"
line_number=-1

#remove dest folder clone the branch and check if exsits  
log "${green}INFO: clear $ansible_root_folder${cyan}"
rm -rf $ansible_root_folder
	echo "clone git ${git_url} to ${ansible_root_folder} folder"
git clone $deploy_url $ansible_root_folder

INPUT=/tmp/yoram/testing/Environment_Information_Abacus.csv

sed -i -e "s#<GIT_URL>#$git_url#g" $f_main

OLDIFS=$IFS
IFS=','
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

while read env_name server_name server_ip site code status garbage
do

	log "${reset} INFO:start processing csv line number:$line_number "
	((line_number++))
	if [ $line_number -eq 0 ]; then
		continue
	fi
	server_name=$(echo "${server_name}" | awk '{print tolower($0)}') 
	server_ip=$(echo "${server_ip}" | awk '{print tolower($0)}')
	status=$(echo "${status}" | awk '{print tolower($0)}')
	if [ -z $server_ip ] ; then
		server_ip=$(ping -c 1 $server_name | gawk -F'[()]' '/PING/{print $2}')
	fi

	if [[ $server_name == *"lmpa" ]]|| [[ $server_name == *"gw" ]]; then
		if [[ $status == "active"* ]]; then
			echo "server_name:$server_name"
			echo "server_ip:$server_ip"
			echo "status:$status"
			sed -i -e "s/localhost/$server_ip/g;s/ansible_connection=local/ansible_user=root/g" $f_inventory
			#replace the <HOST_NAME> and <GIT_URL>
			sed -i -e "s/<HOST_NAME>/$server_name/g" $f_main
		
			#copy sshkey to wl server
			sshpass -p "$workingpass" ssh-copy-id $server_name
			cd  $ansible_root_folder		
			sudo ansible-playbook -i inventory site.yml --tags "lmpa_gw_conf_backup"
		fi
	fi



done < $INPUT

IFS=$OLDIFS